# How to setup Development Environment

_I used ubuntu 16.04_

## Install docker (on ubuntu)

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo systemctl enable docker
sudo systemctl enable containerd
sudo docker ps

sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker 
docker ps
```

## Start mysql (w/ docker)

```bash
cd ~
mkdir mysql-data

docker run -d --name mysql \
  --restart=always \
  -v `pwd`/mysql-data:/var/lib/mysql \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=mypassword \
  mysql:8.0

sudo apt install mysql-client
mysql -h 127.0.0.1 -u root -pmypassword -e "show databases"
```

## Start mongodb

```bash
mkdir -p $HOME/mongo-data

docker run -d -p 27017:27017 --name mongo \
    -e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
    -e MONGO_INITDB_ROOT_PASSWORD=mypassword \
    -v $HOME/mongo-data:/data/db \
    mongo:4.2

cat <<EOF >$HOME/bin/mongo
#!/bin/sh
exec docker run --rm -it --network=host mongo:4.2 mongo "\$@"
EOF

chmod 755 $HOME/bin/mongo

mongo --host 127.0.0.1 -u mongoadmin -p mypassword --authenticationDatabase admin test
```

## Start redis

```bash
mkdir -p $HOME/redis-dir/data

cat <<EOF >$HOME/redis-dir/redis.conf
tcp-backlog 511
timeout 0
tcp-keepalive 10
databases 256
save 300 1
save 100 5
save 30 25
dbfilename dump.rdb
dir /data
maxclients 10000
maxmemory 4294967296
maxmemory-policy noeviction
slowlog-log-slower-than 10000
slowlog-max-len 128
requirepass mypassword
EOF

docker run -d -p 6379:6379 --name redis \
  -v $HOME/redis-dir/data:/data \
  -v $HOME/redis-dir/redis.conf:/redis.conf \
  redis:5.0 \
  redis-server /redis.conf
```

## Start elasticsearch

```bash
mkdir -p $HOME/es-dir/conf
mkdir -p $HOME/es-dir/data

cat <<EOF >$HOME/es-dir/conf/elasticsearch.yml
cluster.name: es
network.host: 0.0.0.0
discovery.type: single-node
xpack.security.enabled: true
EOF

docker run --rm \
  -v $HOME/es-dir/data:/usr/share/elasticsearch/data \
 elasticsearch:7.10.1 \
 sh -c "chown elasticsearch:elasticsearch /usr/share/elasticsearch/data"

docker run -d --name es \
 -p 9200:9200 -p 9300:9300 \
 -v $HOME/es-dir/conf/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
 -v $HOME/es-dir/data:/usr/share/elasticsearch/data \
 elasticsearch:7.10.1

docker exec -it es elasticsearch-setup-passwords auto

curl -u elastic http://127.0.0.1/_cat/nodes
```

## Start nginx

```bash
mkdir -p $HOME/nginx-dir/conf
mkdir -p $HOME/nginx-dir/html
mkdir -p $HOME/nginx-dir/entrypoint

cat <<EOF >$HOME/nginx-dir/conf/default.conf
server {
  listen       80;
  listen  [::]:80;
  server_name  localhost;

  location / {
    gzip on;
    gzip_min_length 1k;
    gzip_comp_level 1;
    gzip_vary on;
    gzip_disable "MSIE [1-6]\.";
    gzip_buffers 32 4k;
    gzip_types
      text/plain
      application/javascript
      application/x-javascript
      text/css
      application/xml
      text/javascript
      application/x-httpd-php
      image/jpeg
      image/gif
      image/png
      application/vnd.ms-fontobject
      font/ttf
      font/opentype
      font/x-woff
      image/svg+xml;

    root      /usr/share/nginx/html;
    try_files \$uri /index.html;
  }
}
EOF

cat <<EOF >$HOME/nginx-dir/html/index.html
test index.html
EOF

cat <<EOF >$HOME/nginx-dir/entrypoint/90-add-base-href.sh
if [ "x\$BASE_HREF" != "x" ]; then
  sed -e "/<head>/a <base href=\\"\$BASE_HREF\\">" -i /usr/share/nginx/html/index.html
fi
EOF

docker run -d --network=host --name nginx \
  -v $HOME/nginx-dir/conf:/etc/nginx/conf.d \
  -v $HOME/nginx-dir/html:/usr/share/nginx/html \
  nginx:1.21-alpine

```

