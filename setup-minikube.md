# How to setup minikube

_I used ubuntu 16.04_

```bash
# Install binaries
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo mv minikube-linux-amd64 /usr/local/bin/minikube
sudo mv kubectl /usr/local/bin/kubectl
sudo chmod 755 /usr/local/bin/minikube /usr/local/bin/kubectl

# Install minikube (driver=none)
sudo apt-get update
sudo apt-get install conntrack
sudo sysctl fs.protected_regular=0
sudo minikube start --driver=none

# Post install configuration (for non-root user)
sudo cp -r /root/.kube $HOME
sudo cp -r /root/.minikube $HOME
sudo chown -R "$USER" $HOME/.kube $HOME/.minikube
sed -i -e 's!/root!'$HOME'!' $HOME/.kube/config

# Test it
alias k=kubectl
k get nodes
```

## Access Registry on Aliyun

1. Find out public IP (NAT IP)
```bash
curl ifconfig.me
```

2. Config ACL of Aliyun Registry

3. Test
